EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "tpwkbd"
Date "2020-11-25"
Rev "1"
Comp "XenGi"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L conn-tp-kbd:conn-tp-kbd U1
U 1 1 5FBA94A4
P 2800 3100
F 0 "U1" H 2800 4515 50  0000 C CNN
F 1 "conn-tp-kbd" H 2800 4424 50  0000 C CNN
F 2 "AA01B-S040VA1:AA01B-S040VA1" H 2800 3100 50  0001 C CNN
F 3 "DOCUMENTATION" H 2800 3100 50  0001 C CNN
	1    2800 3100
	1    0    0    -1  
$EndComp
$Comp
L teensy:Teensy++2.0_(C) U2
U 1 1 5FBBB15D
P 7000 3100
F 0 "U2" H 7000 4737 60  0000 C CNN
F 1 "Teensy++2.0_(C)" H 7000 4631 60  0000 C CNN
F 2 "teensy:Teensy2.0++" H 7000 1600 60  0000 C CNN
F 3 "" H 7100 2250 60  0000 C CNN
	1    7000 3100
	1    0    0    -1  
$EndComp
Text GLabel 1950 2150 0    50   Input ~ 0
ROW5
Text GLabel 1950 2250 0    50   Input ~ 0
ROW0
Text GLabel 1950 2350 0    50   Input ~ 0
ROW3
Text GLabel 1950 2450 0    50   Input ~ 0
ROW2
Text GLabel 1950 2550 0    50   Input ~ 0
ROW4
Text GLabel 1950 2650 0    50   Input ~ 0
ROW1
Text GLabel 1950 2750 0    50   Input ~ 0
ROW6
Text GLabel 1950 2850 0    50   Input ~ 0
ROW7
Text GLabel 3650 2050 2    50   Input ~ 0
COL4
Text GLabel 3650 2150 2    50   Input ~ 0
COL5
Text GLabel 3650 2250 2    50   Input ~ 0
COL8
Text GLabel 3650 2350 2    50   Input ~ 0
COL6
Text GLabel 3650 2450 2    50   Input ~ 0
COL3
Text GLabel 3650 2550 2    50   Input ~ 0
COl7
Text GLabel 3650 2650 2    50   Input ~ 0
COL2
Text GLabel 3650 2750 2    50   Input ~ 0
COL10
Text GLabel 3650 2850 2    50   Input ~ 0
COL1
Text GLabel 3650 2950 2    50   Input ~ 0
COL9
Text GLabel 3650 3050 2    50   Input ~ 0
COL0
Text GLabel 3650 3150 2    50   Input ~ 0
COL11
Text GLabel 3650 3250 2    50   Input ~ 0
COL14
Text GLabel 3650 3350 2    50   Input ~ 0
COL12
Text GLabel 3650 3450 2    50   Input ~ 0
COL15
Text GLabel 3650 3550 2    50   Input ~ 0
COL13
$Comp
L power:GND #PWR0101
U 1 1 5FC157DA
P 1500 4250
F 0 "#PWR0101" H 1500 4000 50  0001 C CNN
F 1 "GND" H 1505 4077 50  0000 C CNN
F 2 "" H 1500 4250 50  0001 C CNN
F 3 "" H 1500 4250 50  0001 C CNN
	1    1500 4250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5FC15BFF
P 4250 4250
F 0 "#PWR0102" H 4250 4000 50  0001 C CNN
F 1 "GND" H 4255 4077 50  0000 C CNN
F 2 "" H 4250 4250 50  0001 C CNN
F 3 "" H 4250 4250 50  0001 C CNN
	1    4250 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 2150 1950 2150
Wire Wire Line
	2050 2250 1950 2250
Wire Wire Line
	2050 2350 1950 2350
Wire Wire Line
	2050 2450 1950 2450
Wire Wire Line
	2050 2550 1950 2550
Wire Wire Line
	2050 2650 1950 2650
Wire Wire Line
	2050 2750 1950 2750
Wire Wire Line
	2050 2850 1950 2850
Wire Wire Line
	1500 4250 1500 4150
Wire Wire Line
	1500 4050 1500 4150
Connection ~ 1500 4150
Wire Wire Line
	4250 4150 4250 4250
Wire Wire Line
	4250 4050 4250 4150
Connection ~ 4250 4150
Wire Wire Line
	3550 3550 3650 3550
Wire Wire Line
	3550 3450 3650 3450
Wire Wire Line
	3550 3350 3650 3350
Wire Wire Line
	3550 3250 3650 3250
Wire Wire Line
	3550 3150 3650 3150
Wire Wire Line
	3550 3050 3650 3050
Wire Wire Line
	3550 2950 3650 2950
Wire Wire Line
	3550 2850 3650 2850
Wire Wire Line
	3550 2750 3650 2750
Wire Wire Line
	3550 2650 3650 2650
Wire Wire Line
	3550 2550 3650 2550
Wire Wire Line
	3550 2050 3650 2050
Wire Wire Line
	3650 2150 3550 2150
Wire Wire Line
	3550 2250 3650 2250
Wire Wire Line
	3650 2350 3550 2350
Wire Wire Line
	3550 2450 3650 2450
Wire Wire Line
	1500 3550 1500 4050
Connection ~ 1500 4050
$Comp
L power:GND #PWR0103
U 1 1 5FC1F1E8
P 8450 4550
F 0 "#PWR0103" H 8450 4300 50  0001 C CNN
F 1 "GND" H 8455 4377 50  0000 C CNN
F 2 "" H 8450 4550 50  0001 C CNN
F 3 "" H 8450 4550 50  0001 C CNN
	1    8450 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5FC2194C
P 5500 1800
F 0 "#PWR0104" H 5500 1550 50  0001 C CNN
F 1 "GND" H 5505 1627 50  0000 C CNN
F 2 "" H 5500 1800 50  0001 C CNN
F 3 "" H 5500 1800 50  0001 C CNN
	1    5500 1800
	1    0    0    -1  
$EndComp
NoConn ~ 8050 2150
NoConn ~ 5950 3950
NoConn ~ 8050 1750
NoConn ~ 8050 1850
Text GLabel 1950 3850 0    50   Input ~ 0
TP_DATA
Text GLabel 1950 3950 0    50   Input ~ 0
TP_CLK
Wire Wire Line
	1950 3850 2050 3850
Wire Wire Line
	2050 3950 1950 3950
Text GLabel 5850 4050 0    50   Input ~ 0
ROW5
Text GLabel 5850 4150 0    50   Input ~ 0
ROW0
Text GLabel 5850 4250 0    50   Input ~ 0
ROW3
Text GLabel 5850 4350 0    50   Input ~ 0
ROW2
Text GLabel 5850 4450 0    50   Input ~ 0
ROW4
Text GLabel 8150 4450 2    50   Input ~ 0
ROW1
Text GLabel 8150 4350 2    50   Input ~ 0
ROW6
Text GLabel 8150 4250 2    50   Input ~ 0
ROW7
Text GLabel 5850 1850 0    50   Input ~ 0
COL4
Text GLabel 5850 1950 0    50   Input ~ 0
COL5
Text GLabel 5850 2050 0    50   Input ~ 0
COL8
Text GLabel 5850 2250 0    50   Input ~ 0
COL6
Text GLabel 5850 2550 0    50   Input ~ 0
COL3
Text GLabel 5850 2650 0    50   Input ~ 0
COl7
Text GLabel 5850 2750 0    50   Input ~ 0
COL2
Text GLabel 5850 2850 0    50   Input ~ 0
COL10
Text GLabel 5850 2950 0    50   Input ~ 0
COL1
Text GLabel 5850 3050 0    50   Input ~ 0
COL9
Text GLabel 5850 3150 0    50   Input ~ 0
COL0
Text GLabel 5850 3250 0    50   Input ~ 0
COL11
Text GLabel 5850 3350 0    50   Input ~ 0
COL14
Text GLabel 5850 3450 0    50   Input ~ 0
COL12
Text GLabel 5850 3550 0    50   Input ~ 0
COL15
Text GLabel 5850 3650 0    50   Input ~ 0
COL13
Text GLabel 4650 6600 3    50   Input ~ 0
TP_DATA
Text GLabel 4650 6000 1    50   Input ~ 0
TP_CLK
Text GLabel 1950 3050 0    50   Input ~ 0
LED_CAPS
Text GLabel 1950 3150 0    50   Input ~ 0
LED_PWR
$Comp
L power:+5V #PWR0106
U 1 1 5FC354B0
P 1400 3650
F 0 "#PWR0106" H 1400 3500 50  0001 C CNN
F 1 "+5V" H 1415 3823 50  0000 C CNN
F 2 "" H 1400 3650 50  0001 C CNN
F 3 "" H 1400 3650 50  0001 C CNN
	1    1400 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 3650 1400 3750
Wire Wire Line
	1950 3050 2050 3050
Wire Wire Line
	2050 3150 1950 3150
Text GLabel 5000 5150 0    50   Input ~ 0
LED_CAPS
Text GLabel 5000 5250 0    50   Input ~ 0
LED_PWR
Text GLabel 8150 3350 2    50   Input ~ 0
BLE_CS
Text GLabel 8150 3950 2    50   Input ~ 0
BLE_IRQ
Wire Wire Line
	5950 2350 5850 2350
Wire Wire Line
	8050 3950 8150 3950
Text GLabel 8150 3450 2    50   Input ~ 0
MISO
Text GLabel 8150 3550 2    50   Input ~ 0
MOSI
Text GLabel 8150 3650 2    50   Input ~ 0
SCK
Wire Wire Line
	8050 3350 8150 3350
Wire Wire Line
	8050 3450 8150 3450
Wire Wire Line
	8050 3550 8150 3550
Wire Wire Line
	8050 3650 8150 3650
$Comp
L Connector:Conn_01x09_Female J1
U 1 1 5FC8605E
P 10400 2700
F 0 "J1" H 10428 2726 50  0000 L CNN
F 1 "Conn_01x09_Female" H 10428 2635 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x09_P2.54mm_Horizontal" H 10400 2700 50  0001 C CNN
F 3 "~" H 10400 2700 50  0001 C CNN
	1    10400 2700
	1    0    0    -1  
$EndComp
Text GLabel 10100 2300 0    50   Input ~ 0
SCK
Text GLabel 10100 2400 0    50   Input ~ 0
MISO
Text GLabel 10100 2500 0    50   Input ~ 0
MOSI
Text GLabel 10100 2600 0    50   Input ~ 0
BLE_CS
Text GLabel 10100 2700 0    50   Input ~ 0
BLE_IRQ
NoConn ~ 10200 2800
Text GLabel 10100 2900 0    50   Input ~ 0
BLE_RESET
$Comp
L power:GND #PWR0108
U 1 1 5FC8AC4E
P 10100 3200
F 0 "#PWR0108" H 10100 2950 50  0001 C CNN
F 1 "GND" H 10105 3027 50  0000 C CNN
F 2 "" H 10100 3200 50  0001 C CNN
F 3 "" H 10100 3200 50  0001 C CNN
	1    10100 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0109
U 1 1 5FC8B77E
P 9550 3000
F 0 "#PWR0109" H 9550 2850 50  0001 C CNN
F 1 "+5V" H 9565 3173 50  0000 C CNN
F 2 "" H 9550 3000 50  0001 C CNN
F 3 "" H 9550 3000 50  0001 C CNN
	1    9550 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 3000 10100 3000
Wire Wire Line
	10100 3000 10100 3200
Wire Wire Line
	10100 2900 10200 2900
Wire Wire Line
	10100 2700 10200 2700
Wire Wire Line
	10100 2600 10200 2600
Wire Wire Line
	10100 2500 10200 2500
Wire Wire Line
	10100 2400 10200 2400
Wire Wire Line
	10100 2300 10200 2300
NoConn ~ 8050 4150
Wire Wire Line
	5950 2150 5850 2150
Wire Wire Line
	5850 2450 5950 2450
$Comp
L power:+5V #PWR0112
U 1 1 5FC8B0D6
P 4350 3750
F 0 "#PWR0112" H 4350 3600 50  0001 C CNN
F 1 "+5V" H 4365 3923 50  0000 C CNN
F 2 "" H 4350 3750 50  0001 C CNN
F 3 "" H 4350 3750 50  0001 C CNN
	1    4350 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 4050 8450 4550
Wire Wire Line
	8050 4050 8450 4050
Wire Wire Line
	5850 1850 5950 1850
Wire Wire Line
	5850 1950 5950 1950
Wire Wire Line
	5850 2050 5950 2050
Wire Wire Line
	5850 2250 5950 2250
Wire Wire Line
	5850 2550 5950 2550
Wire Wire Line
	5850 2650 5950 2650
Wire Wire Line
	5850 2750 5950 2750
Wire Wire Line
	5850 2850 5950 2850
Wire Wire Line
	5850 2950 5950 2950
Wire Wire Line
	5850 3050 5950 3050
Wire Wire Line
	5850 3150 5950 3150
Wire Wire Line
	5950 3250 5850 3250
Wire Wire Line
	5850 3350 5950 3350
Wire Wire Line
	5950 3450 5850 3450
Wire Wire Line
	5850 3550 5950 3550
Wire Wire Line
	5850 3650 5950 3650
Wire Wire Line
	5850 4050 5950 4050
Wire Wire Line
	5850 4150 5950 4150
Wire Wire Line
	5850 4250 5950 4250
Wire Wire Line
	5850 4350 5950 4350
Wire Wire Line
	5850 4450 5950 4450
Wire Wire Line
	8050 4250 8150 4250
Wire Wire Line
	8050 4450 8150 4450
Wire Wire Line
	8050 4350 8150 4350
Wire Wire Line
	4650 6250 4650 6300
Connection ~ 4650 6300
Text GLabel 3700 3950 2    50   Input ~ 0
TP_RESET
Wire Wire Line
	3550 3950 3700 3950
Text GLabel 5300 6300 2    50   Input ~ 0
TP_RESET
$Comp
L Device:R_Small R2
U 1 1 602470B7
P 4650 6150
F 0 "R2" H 4709 6196 50  0000 L CNN
F 1 "4.7K" H 4709 6105 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" H 4650 6150 50  0001 C CNN
F 3 "~" H 4650 6150 50  0001 C CNN
	1    4650 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 6024EB20
P 4650 6450
F 0 "R1" H 4709 6496 50  0000 L CNN
F 1 "4.7K" H 4709 6405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" H 4650 6450 50  0001 C CNN
F 3 "~" H 4650 6450 50  0001 C CNN
	1    4650 6450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 602640EF
P 5250 6600
F 0 "#PWR0107" H 5250 6350 50  0001 C CNN
F 1 "GND" H 5255 6427 50  0000 C CNN
F 2 "" H 5250 6600 50  0001 C CNN
F 3 "" H 5250 6600 50  0001 C CNN
	1    5250 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 6600 5250 6550
$Comp
L power:+5V #PWR0113
U 1 1 60276FA3
P 4300 6250
F 0 "#PWR0113" H 4300 6100 50  0001 C CNN
F 1 "+5V" H 4315 6423 50  0000 C CNN
F 2 "" H 4300 6250 50  0001 C CNN
F 3 "" H 4300 6250 50  0001 C CNN
	1    4300 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R3
U 1 1 60280981
P 5250 6450
F 0 "R3" H 5309 6496 50  0000 L CNN
F 1 "100K" H 5309 6405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" H 5250 6450 50  0001 C CNN
F 3 "~" H 5250 6450 50  0001 C CNN
	1    5250 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 6300 5000 6300
Wire Wire Line
	5300 6300 5250 6300
Wire Wire Line
	5250 6350 5250 6300
Connection ~ 5250 6300
Wire Wire Line
	5250 6300 5200 6300
Wire Wire Line
	4650 6350 4650 6300
Wire Wire Line
	4300 6250 4300 6300
Wire Wire Line
	4300 6300 4650 6300
Text GLabel 1950 3650 0    50   Input ~ 0
LED_MUTE
Wire Wire Line
	1950 3650 2050 3650
Wire Wire Line
	1500 3550 2050 3550
Wire Wire Line
	1400 3750 2050 3750
Wire Wire Line
	1500 4050 2050 4050
Wire Wire Line
	1500 4150 2050 4150
Text GLabel 5000 5050 0    50   Input ~ 0
LED_MUTE
Text GLabel 3650 3750 2    50   Input ~ 0
LED_MICMUTE
Text GLabel 5000 4950 0    50   Input ~ 0
LED_MICMUTE
Wire Wire Line
	3550 3750 3650 3750
Wire Wire Line
	4350 3850 4350 3750
Wire Wire Line
	3550 3850 4350 3850
Wire Wire Line
	3550 3650 4250 3650
Wire Wire Line
	4250 3650 4250 4050
Connection ~ 4250 4050
Wire Wire Line
	3550 4050 4250 4050
Wire Wire Line
	3550 4150 4250 4150
Text GLabel 1950 2050 0    50   Input ~ 0
HOTKEY
Wire Wire Line
	1950 2050 2050 2050
Text GLabel 8150 3850 2    50   Input ~ 0
HOTKEY
Text GLabel 1950 2950 0    50   Input ~ 0
PWRSWITCH
Wire Wire Line
	2050 2950 1950 2950
Text GLabel 8150 3750 2    50   Input ~ 0
PWRSWITCH
NoConn ~ 2050 3250
NoConn ~ 2050 3350
NoConn ~ 2050 3450
$Comp
L power:GND #PWR0115
U 1 1 6035D89E
P 5450 3900
F 0 "#PWR0115" H 5450 3650 50  0001 C CNN
F 1 "GND" H 5455 3727 50  0000 C CNN
F 2 "" H 5450 3900 50  0001 C CNN
F 3 "" H 5450 3900 50  0001 C CNN
	1    5450 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3850 5450 3850
Wire Wire Line
	5450 3850 5450 3900
Wire Wire Line
	5950 1750 5500 1750
Wire Wire Line
	5500 1750 5500 1800
Text GLabel 5850 2350 0    50   Input ~ 0
BLE_RESET
Wire Wire Line
	9550 3000 9550 3100
Wire Wire Line
	9550 3100 10200 3100
$Comp
L Device:C_Small C1
U 1 1 60380160
P 5100 6300
F 0 "C1" V 4871 6300 50  0000 C CNN
F 1 "2.2uF" V 4962 6300 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 5100 6300 50  0001 C CNN
F 3 "~" H 5100 6300 50  0001 C CNN
	1    5100 6300
	0    1    1    0   
$EndComp
Wire Wire Line
	8500 3050 8500 2950
$Comp
L power:+5V #PWR0105
U 1 1 5FC23773
P 8500 2950
F 0 "#PWR0105" H 8500 2800 50  0001 C CNN
F 1 "+5V" H 8515 3123 50  0000 C CNN
F 2 "" H 8500 2950 50  0001 C CNN
F 3 "" H 8500 2950 50  0001 C CNN
	1    8500 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 3050 8500 3050
NoConn ~ 5950 3750
Text GLabel 5850 2450 0    50   Input ~ 0
TP_CLK
Wire Wire Line
	4650 6050 4650 6000
Wire Wire Line
	4650 6550 4650 6600
Text GLabel 5850 2150 0    50   Input ~ 0
TP_DATA
Text GLabel 8150 3250 2    50   Input ~ 0
LED_CAPS
Wire Wire Line
	8050 3250 8150 3250
Wire Wire Line
	8050 3750 8150 3750
Wire Wire Line
	8050 3850 8150 3850
$EndSCHEMATC
