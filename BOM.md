BOM
===

_all prices are in EUR_

## PCB components

| part   | value | lcsc    |  #  | price  | total |
| ------ | ----- | ------- | --- | ------ | ----- |
| R1, R2 | 4.7K  | C433513 |  2  | 0.0081 |  0.41 | (minimum 50)
| R3     | 100K  | C433530 |  1  | 0.0084 |  0.42 | (minimum 50)
| C1     | 2.2uF | C49315  |  1  | 0.1468 |  0.15 |

## Other components

| part                             | shop        | art-nr       |  #  | price  | total  |
| -------------------------------- | ----------- | ------------ | --- | ------ | ------ |
| Adafruit Bluefruit LE SPI Friend | exp-tech.de | EXP-R15-831  |  1  |  20.07 |  20.07 |
| PJRC Teensy++ 2.0                | exp-tech.de | EXP-R05-440  |  1  |  27.84 |  27.84 |
| Lenovo ThinkPad X220 Keyboard    | ~           | ~            |  1  | ~40.00 | ~40.00 |
| USB 2.0 Type-C Breakout          | exp-tech.de | EXP-R25-1493 |  1  |   3.21 |   3.21 |
| Mini USB Cable                   | exp-tech.de | EXP-R27-034  |  1  |   1.40 |   1.40 |
